/*********************************************************
 * Base game image class for bitmapped game entities
 **********************************************************/
import java.awt.*;
import java.awt.geom.*;
import java.net.*;

public class ImageEntity extends Sprite {
    //variables
    protected Image image;
    protected String filename;
    protected AffineTransform at;
    
    //default constructor
    public ImageEntity() {
       
       
    }
    
    public ImageEntity(int pLocx, int pLocy, int pVelx, int pVelY, String pFileName, boolean pIsActive, boolean pIsVisible)
    {
    	super();
    	setLocX(pLocx);
    	setLocY(pLocy);
    	setVelX(pVelx);
    	setVelY(pVelY);
    	filename = pFileName;
    	loadImage();
    	setVisible(pIsVisible);
    	setActive(pIsActive);
    }
    

    public Image getImage() { 
    	return image;
    }

    public void setImage(Image image) {
        this.image = image;
        
    }

    
    public double getCenterX() {
        return getLocX() + image.getWidth(null) / 2.0;
    }
    public double getCenterY() {
        return getLocY() + image.getHeight(null) / 2.0;
    }

 
   

    public void loadImage() {
    	URL url = null;
    	Toolkit tk = Toolkit.getDefaultToolkit();
        url = this.getClass().getResource(filename);
        image = tk.getImage(url);
    }

   /* public void transform() {

    	
    	
    }
*/
   
    //bounding rectangle
    public Rectangle getBoundingBox() {
        Rectangle r;
        r = new Rectangle((int)getLocX(), (int)getLocY(), image.getWidth(null), image.getHeight(null));
        return r;
    }

	@Override
	public void draw(Graphics g) {
		Graphics2D g2d = (Graphics2D)g;
		
		g2d.drawImage(image, getLocX(), getLocY(), null);
		
	}
}
